package org.levelup.lesson2;

import java.util.Random;
import java.util.Scanner;

public class GuessNumberGame {

    public static void main(String[] args) {
        // GuessNumberGame game = new GuessNumberGame();

        int secretNumber = 0;
        int inputNumber = 10;

        // Ввод числа с клавиатуры
        System.out.println("Введите число");
        Scanner in = new Scanner(System.in);
        inputNumber = in.nextInt();

        // Сгенерируем псевдослучайное число
        Random randomizer = new Random();
        // nextInt() - [0, 32)
        // nextInt(5) - [0, 5)
        secretNumber = randomizer.nextInt(3);

//        while (true) {
//            while (true) {
//                break;
//            }
//        }

        while (true) {
            // == - сравнение двух чисел, вернет true, если числа равны
            if (inputNumber == secretNumber) {
                // int a = 30;
                System.out.println("Вы угадали число!");
                // int value = 0;
                break;
            } else {
                //int a = 40;
                // value = 3;
                // конкатенация строк
                // System.out.println("Вы не угадали число! Загаданное число: " + secretNumber);
                if (inputNumber > secretNumber) {
                    System.out.println("Вы ввели число, которое больше загаданного");
                } else {
                    System.out.println("Вы ввели число, которое меньше загаданного");
                }
                inputNumber = in.nextInt();
            }
        }

//        if (inputNumber == secretNumber) {
//            System.out.println();
//        } else if (inputNumber > secretNumber) {
//            System.out.println();
//        } else {
//
//        }
//
//        Вот так делать не стоит :)
//        if (inputNumber == secretNumber) {
//            System.out.println();
//        }
//        if (inputNumber > secretNumber) {
//            System.out.println();
//        }
//        if (inputNumber < secretNumber) {
//            System.out.println();
//        }
    }

}
