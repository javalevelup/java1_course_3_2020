package org.levelup.lesson6.figure;

public class Triangle extends Shape {

    public Triangle() {
        super();
    }

    public Triangle(int param) {
        super(param);
    }

    @Override
    public double calculateSquare() {
        return 12;
    }

}
