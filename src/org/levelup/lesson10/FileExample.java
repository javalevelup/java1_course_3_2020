package org.levelup.lesson10;

import java.io.File;
import java.io.IOException;

public class FileExample {

    public static void main(String[] args) throws IOException {

        // Каждый файл содержит:
        //  - имя
        //  - путь (к файлу)
        //      - абсолютный путь - /Users/dmitrijprocko/IdeaProjects/java1_course_3_2020/src/org/levelup/lesson10/FileExample.java
        //      - относительный путь - /src/org/levelup/lesson10/FileExample.java

        File testFile = new File("test-file.txt");
        File notExistingFile = new File("not-test-file.txt");

        // Для проверки, что файл существует
        boolean isTestFileExist = testFile.exists();
        boolean isNotTestFileExist = notExistingFile.exists();

        System.out.println(isTestFileExist);
        System.out.println(isNotTestFileExist);

        boolean isCreated = notExistingFile.createNewFile();
        System.out.println("Создали файл: " + isCreated);

        // rwe
        // readable
        // writeable
        // executable

        File srcDir = new File("src");
        if (srcDir.isDirectory()) {
            System.out.println("src - это папка");
        }

        File testSrcDir = new File("test/src");
        boolean isDirsCreated = testSrcDir.mkdirs();
        System.out.println("Папки созданы: " + isDirsCreated);

    }

}
