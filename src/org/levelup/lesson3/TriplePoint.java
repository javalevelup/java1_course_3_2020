package org.levelup.lesson3;

// private - недоступен везде, кроме класса, в котором объявлен
// default-package (private-package) - доступ разрешен внутри пакета
// protected - доступ разрешен внутри пакета, но и еще в классах-наследниках (даже если находятся в других пакетах)
// public - можно обращаться из любой точки программы

// Наследование (inheritance)
// Тот класс, от которого унаследовались (Point) - суперкласс, базовый класс, родитель, parent class
// Тот класс, который унаследовался (TriplePoint) - подкласс, subclass, ребенок
public class TriplePoint extends Point {

    // int x;
    private int z;

    public TriplePoint() {
        super(); // вызов констркуктора суперкласса
        System.out.println("Constructor TriplePoint...");
    }

    TriplePoint(int x, int y, int z) {
        super(x, y);
        // this.x = x;
        // this.y = y;
        this.z = z;
    }

    public void changeCoordinates(int x, int y, int z) {
        //super.x = x;
        this.x = x;
        super.y = y;
        this.z = z;
    }

    // setter
    // public void set<FieldName>(<field type> <fieldName>) { this.<fieldName> = <fieldName>; }
    public void setZ(int z) {
        this.z = z;
    }

}