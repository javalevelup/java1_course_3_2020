package org.levelup.lesson9;

import java.util.LinkedList;

public class ForeachLoop {

    public static void main(String[] args) {
        LinkedList<Integer> linkedList = new LinkedList<>();

        linkedList.add(3); // linkedList.add(Integer.valueOf(3));
        linkedList.add(5);
        linkedList.add(7);
        linkedList.add(null);
        linkedList.add(7);
        linkedList.add(8);

        // for (int el : linkedList) {
        for (Integer el : linkedList) {
            System.out.println(el);
        }

    }

}
