package org.levelup.lesson9.enumerations;

import org.levelup.structure.LinkList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

// javasist
public class AICExamples {

    public static void main(String[] args) {
        Comparator<String> comparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return -o1.compareToIgnoreCase(o2);
            }
        };

//        LinkList linkList = new LinkList() {
//            @Override
//            public void add(int value) {
//                super.add(value);
//            }
//        };

        // Comparator<String> comparatorLambda = (o1, o2) -> -o1.compareToIgnoreCase(o2);
        Collections.sort(new ArrayList<>(), comparator);
    }

}
