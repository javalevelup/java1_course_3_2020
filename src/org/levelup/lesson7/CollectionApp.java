package org.levelup.lesson7;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CollectionApp {

    public static void main(String[] args) {
        List<String> names = new ArrayList<>(); // DynamicArray
        // List<String> names = new LinkedList<>(); // LinkList

        names.add("Ivan");
        names.add("Igor");
        names.add("Oleg");
        names.add("Marya");
        names.add("Natalya");
        names.add("Fedor");
        names.add("Dmitry");

        System.out.println(names);

        boolean doesOlegExist = names.contains("Oleg");
        System.out.println("Is Oleg exist: " + doesOlegExist);

        boolean doesAlexeyExist = names.contains("Alexey"); // for () { if ("Alexey".equals(arr[i])) }
        System.out.println("Is Alexey exist: " + doesAlexeyExist);

        if (!names.isEmpty()) {
            System.out.println("Fifth name: " + names.get(4));
        }

        int index = names.indexOf("Dmitry");
        System.out.println(index);
        System.out.println(names.indexOf("Vasiliy"));

        // foreach
        // for (<type of array/collection> varname : <array/collection>) {}
        for (String name : names) {
            System.out.println(name);
        }

        System.out.println();
        for (String name : names) {
            if (name.length() >= 5) {
                System.out.println(name);
            }
        }

    }

}
