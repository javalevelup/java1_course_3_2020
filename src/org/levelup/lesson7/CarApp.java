package org.levelup.lesson7;

@SuppressWarnings("ALL")
public class CarApp {

    public static void main(String[] args) {
        Car car = new Car();
        car.go();

        Car[] cars = new Car[3]; // java.lang.reflect.Array
        for (int i = 0; i < cars.length; i++) {
            cars[i].go();
        }
    }

}
