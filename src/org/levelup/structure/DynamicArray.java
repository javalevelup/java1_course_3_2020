package org.levelup.structure;

// Динамический массив - список на основе массива
// Массив, у которого может изменяться длина
public class DynamicArray extends AbstractStructure implements Structure {

    private int[] elements;

    public DynamicArray() {
        this(10);
    }

    // initialCapacity - первоначальный размер массива
    public DynamicArray(int initialCapacity) {
        this.elements = new int[initialCapacity];
    }

    // while (true) { da.add(1); }
    @Override
    public void add(int value) {
        if (elements.length == size) {
            // Увеличить размер массива
            int[] oldElements = elements;
            elements = new int[(int)(elements.length * 1.5)];
            // [0..0], l = 10
            // [0..0, 0], l = 11
            // [0..0, 0, 0], l = 12
            System.arraycopy(oldElements, 0, elements, 0, oldElements.length);
        }
        // [0, 0, 0, 0, 0]
        // [7, 0, 0, 0, 0]
        // [7, 9, 0, 0, 0]
        // [7, 9, 4, 0, 0]
        elements[size++] = value; // elements[size] = value; size++;
        // add(5)
        // size = 0; elements[0] = 5; [5, 0, 0, 0, 0], size = 1
        // add(9)
        // size = 1; elements[1] = 9; [5, 9, 0, 0, 0], size = 9
    }

}
