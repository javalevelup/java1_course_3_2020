package org.levelup.lesson5;

import org.levelup.lesson3.Point;

@SuppressWarnings("ALL")
public class PointComparison {

    public static void main(String[] args) {
        Point nullPointer = null;

        Point p1 = new Point(4, 5, "A"); // ссылка1 12304
        Point p2 = new Point(4, 5, "A"); // ссылка2 54538
        Point p3 = p1; // ссылка1 12304 new Point(p1.getX(), p1.getY(), p1.getName())

        // p1 = new Point(5, 6, "B"); // ссылка3 50843

        System.out.println(p1 == p2); // false // org.levelup.lesson3.Point@439dfba
        System.out.println(p1 == p3); // true

        // boolean equals()
        System.out.println(p1.equals(p2)); // true

        // p1.equals("some string");
    }

}
