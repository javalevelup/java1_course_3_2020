package org.levelup.lesson5;

import org.levelup.lesson3.Point;

public class Rectangle extends Shape {

    // int[] arr = {1, 2, 4};
    public Rectangle(Point leftTopPoint, Point rightBottomPoint) {
        // points[] {
        //  0 - left top
        //  1 - right top
        //  2 - left bottom
        //  3 - right bottom
        // }
        super(new Point[] {
                leftTopPoint,
                new Point(leftTopPoint.getY(), rightBottomPoint.getX()), // правая верхняя точка
                new Point(rightBottomPoint.getX(), leftTopPoint.getY()), // левая нижняя точка
                rightBottomPoint
        });
    }

    // Переопределение метода (overriding)
    @Override
    public double calculateSquare() {
        // расстояние между левыми точками
        double width = points[0].calculateDistance(points[2]);
        double height = points[1].calculateDistance(points[3]);

        return width * height;
    }

    public double getWidth() {
        return points[0].calculateDistance(points[2]);
    }

    public double getHeight() {
        return points[1].calculateDistance(points[3]);
    }

}
