package org.levelup.lesson9.enumerations;

import java.util.Comparator;
import java.util.NoSuchElementException;

// enum - разновидность класса (классом)
// 1. Может ли enum наследовать какой-либо класс? Нет
// 2. Может ли enum наследовать абстрактный класс? Нет
// 3. Может ли enum наследовать другой enum? Нет
// 4. Может ли enum реализовывать интерфейс? Да
// 5. Может ли enum иметь абстрактные методы? Да
public enum ApplicationMode implements Comparator<ApplicationMode> {

    DEV("194.53.12.43") { // Anonymous inner class -  org.levelup.lesson9.enumerations.ApplicationMode$1
        @Override
        public String getReplicaHost() {
            return "234.53.231.45";
        }
    },        // объект enum ApplicationMode
    LOCAL("127.0.0.1") { // 2
        @Override
        public String getReplicaHost() {
            return "192.127.0.1";
        }
    };

    private String host; // адрес сервера

    ApplicationMode(String host) {
        this.host = host;
    }

    public String getHost() {
        return host;
    }

    public static ApplicationMode fromHost(String host) {
        ApplicationMode[] modes = values();
        for (ApplicationMode mode : modes) {
            if (mode.getHost().equals(host)) {
                return mode;
            }
        }

        throw new NoSuchElementException();
    }

    @Override
    public int compare(ApplicationMode o1, ApplicationMode o2) {
        return o1.name().compareTo(o2.name());
    }

    public abstract String getReplicaHost();

}
