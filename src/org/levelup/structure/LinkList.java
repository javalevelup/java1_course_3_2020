package org.levelup.structure;

import java.util.Iterator;

// Однонаправленный связный список
public class LinkList extends AbstractStructure implements Structure, Iterable<Integer> {

    // Начало нашего списка (head)
    private Element head;

    @Override
    public void add(int value) {
        Element element = new Element(value); // сразу создаем элемент списка
        if (head == null) { // если head = null, то список пуст
            head = element;
        } else {
            Element current = head; // head: 89575, current: 89575
            while (current.getNext() != null) {
                current = current.getNext();
                // 1 iter: current: 89575  current.next: 45643 -> current: 45643
            }
            // current - ссылается на последний элемент списка
            current.setNext(element);
            // теперь current является предпоследним элементом
        }
        size++;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new LinkListIterator(head);
    }

}
