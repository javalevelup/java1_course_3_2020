package org.levelup.structure;

// Объект списка
public class Element {

    private Element next; // Ссылка на следующий элемент списка
    private int intValue; // Само значение (которое добавляется с помощью метода add(int value))

    public Element(int intValue) {
        this.next = null;
        this.intValue = intValue;
    }

    public Element getNext() {
        return next;
    }

    public void setNext(Element next) {
        this.next = next;
    }

    public int getIntValue() {
        return intValue;
    }

    public void setIntValue(int intValue) {
        this.intValue = intValue;
    }
}