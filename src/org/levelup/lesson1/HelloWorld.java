package org.levelup.lesson1;

public class HelloWorld {

    // psvm + Tab
    public static void main(String[] args) {
        // sout + Tab
        System.out.println("Hello world!");

        // тип переменной <название переменной>
        int first;
        first = 43; // запись значения в переменную

        int second = 78; // инициализировали переменную

        int sum;
        sum = first + second;

        System.out.println(sum);
        System.out.println("Сумма двух переменных " + sum);

    }

}
