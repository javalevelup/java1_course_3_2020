package org.levelup.lesson9.exception;

// unchecked exception
public class StackOverflowException extends RuntimeException {

    private Object newElementInStack; // Тот объект, который мы хотели добавить с stack

    public StackOverflowException(Object obj) {
        super("Пытались добавить этот элемент в стек: " + obj);
        this.newElementInStack = obj;
    }

    public Object getNewElementInStack() {
        return newElementInStack;
    }

}
