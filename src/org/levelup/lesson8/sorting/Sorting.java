package org.levelup.lesson8.sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Sorting {

    public static void main(String[] args) {
        List<User> users = new ArrayList<>();
        users.add(new User("asdf", "Ivan", 34));
        users.add(new User("vasya2", "Vasya", 32));
        users.add(new User("lol", "Leonid", 45));
        users.add(new User("tor", "Petr", 23));
        users.add(new User("hell", "Dmitry", 19));

        // <T extends Comparable<? super T>>
        Collections.sort(users, new UserNameComparator().reversed()); // TimSort
        for (User user : users) {
            System.out.println(user.toString());
        }

        Map<User, String> map = new TreeMap<>(new UserNameComparator()); // red-black tree
        map.put(new User("asdf", "Ivan", 34), "Tank");
        map.put(new User("vasya2", "Vasya", 32), "Priest");
        map.put(new User("lol", "Leonid", 45), "Attacker");
        map.put(new User("tor", "Petr", 23), "Defender");
        map.put(new User("hell", "Dmitry", 19), "Defender");

        System.out.println();
        Set<User> userKeys = map.keySet();
        for (User userKey : userKeys) {
            System.out.println("Key: " + userKey.toString() + ", value: " + map.get(userKey));
        }

    }

}
