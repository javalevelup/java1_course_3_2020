package org.levelup.lesson6.figure;

// Абстрактный класс/метод
// Shape s = new Shape(); - так теперь нельзя!
public abstract class Shape {

    private int param;

    public Shape() {}

    public Shape(int param) {
        System.out.println("Параметр: " + param);
        this.param = param;
    }

    public double calculateSquareWithProfiling() {
        long startTime = System.nanoTime(); // текущее время в наносекундах

        double square = calculateSquare(); // AbstractMethodError

        long endTime = System.nanoTime();
        System.out.println("Расчет площади выполнился за " + (endTime - startTime));

        return square;
    }

    public abstract double calculateSquare();

}
