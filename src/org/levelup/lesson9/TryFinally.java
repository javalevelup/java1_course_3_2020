package org.levelup.lesson9;

@SuppressWarnings("ALL")
public class TryFinally {

    public static void main(String[] args) {
        int r1 = tryFinally(0);
        int r2 = tryFinally(1);

        System.out.println(r1);
        System.out.println(r2);
    }

    static int tryFinally(int value) {
        try {
            if (value == 1) {
                throw new RuntimeException();
            }
            // System.exit(0);
        } catch (Exception exc) {
            return 1;
        } finally {
            System.out.println("qwerqwer");
            return 2;
        }
    }

}
