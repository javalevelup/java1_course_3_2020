package org.levelup.lesson6.figure;

public class App {

    public static void main(String[] args) {
        Triangle tr = new Triangle();
        double square = tr.calculateSquareWithProfiling();
        System.out.println(square);

        // Shape shape = new Triangle();
        // Shape shape = new Shape();
    }

}
