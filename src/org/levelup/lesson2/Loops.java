package org.levelup.lesson2;

public class Loops {

    public static void main(String[] args) {

        // i = i + 1 | i += 1 | i++
        // i = i + 2 | i += 2
        for (int i = 0; i < 10; i = i + 1) {
            System.out.println("We like Java!");
        }

        // for (;;) - бесконечный цикл

        // int i = 0;
        // for (; i < 10; i = i + 1)

        // for (int j = 0; j < 10; ) { j++; }



    }

}
