package org.levelup.lesson7;

import java.util.HashMap;
import java.util.Map;

public class MapApp {

    public static void main(String[] args) {
        // String - key (word)
        // Integer - value (count)
        Map<String, Integer> words = new HashMap<>();
        words.put("the", 23);
        words.put("a", 21);
        words.put("one", 5);
        words.put("two", 7);

        System.out.println(words);
    }

}
