package org.levelup.lesson8;

import org.levelup.structure.LinkList;

import java.util.Iterator;

public class LinkListForeachLoop {

    public static void main(String[] args) {
        LinkList linkList = new LinkList();
        linkList.add(2);
        linkList.add(4);
        linkList.add(5);
        linkList.add(6);
        linkList.add(9);
        linkList.add(2);
        linkList.add(3);

        // for (int el : linkList) {
        for (Integer el : linkList) {
            // el.intValue();
            System.out.println(el);
        }

        System.out.println();

        Iterator<Integer> iterator = linkList.iterator();
        while (iterator.hasNext()) {
            int el = iterator.next();
            System.out.println(el);
        }

    }

}
