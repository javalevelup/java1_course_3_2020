package org.levelup.lesson9.exception;

import java.util.Date;

public class DateApp {

    public static void main(String[] args) {
        // Даты
        //  хранятся в миллисекундах (long), которые прошли с 1 января 1970 (Linux epoch)
        long currentDate = System.currentTimeMillis();
        System.out.println("Текущая дата: " + currentDate);

        // 23.12.2023
        Date now = new Date(); // Текущую дату

        DateParser parser = new DateParser();

        Date date = parser.fromString("25.08.2020 15:65:16");
        System.out.println(date);

    }

}
