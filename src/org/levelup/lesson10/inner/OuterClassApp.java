package org.levelup.lesson10.inner;

public class OuterClassApp {

    public static void main(String[] args) {
        OuterClass.objectName = "outer class object";
        System.out.println(OuterClass.objectName);

        OuterClass.staticMethod();

        OuterClass outerClass = new OuterClass();
        // OuterClass.InnerClass iClass = new OuterClass().new InnerClass();
        OuterClass.InnerClass innerClass = outerClass.new InnerClass();

        innerClass.setPrivateInt(390);
        innerClass.displayPrivateInt();

        outerClass.displayUsingInnerClass();

        OuterClass.NestedClass nestedClass = new OuterClass.NestedClass();
        nestedClass.methodInNestedClass();

        // Локальный класс
        class LocalClass {
            double fResult;
            double sResult;
        }

        LocalClass localClass = new LocalClass();
        localClass.fResult = 43;
        localClass.sResult = 42;

    }

}
