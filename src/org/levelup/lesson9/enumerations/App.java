package org.levelup.lesson9.enumerations;

public class App {

    public static void main(String[] args) {
        new ApplicationStartup().startApplication(ApplicationMode.LOCAL);

        String enumObjectName = ApplicationMode.DEV.name();
        System.out.println(enumObjectName);
        int enumObjectOrdinal = ApplicationMode.DEV.ordinal();
        System.out.println(enumObjectOrdinal);

        ApplicationMode mode = ApplicationMode.valueOf("local".toUpperCase());
        System.out.println(mode);

        ApplicationMode[] modes = ApplicationMode.values();
        for (ApplicationMode m : modes) {
            System.out.println(m.getHost());
        }

        ApplicationMode modeFromHost = ApplicationMode.fromHost("127.0.0.1");
        System.out.println(modeFromHost);

    }

}
