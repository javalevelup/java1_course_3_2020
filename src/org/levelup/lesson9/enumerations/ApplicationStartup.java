package org.levelup.lesson9.enumerations;

public class ApplicationStartup {

    // startApplication("develop");
    public void startApplication(String mode) {
        System.out.println("Application mode: " + mode);
        if (mode.equalsIgnoreCase("dev")) {
            System.out.println("Приложение запущено в dev среде");
        } else if (mode.equalsIgnoreCase("local")) {
            System.out.println("Приложение запускается локально");
        } else {
            throw new IllegalArgumentException();
        }
    }

    // startApplication(ApplicationMode.DEV)
    public void startApplication(ApplicationMode mode) {
//        if (mode == ApplicationMode.DEV) {
//            System.out.println("Приложение запущено в dev среде");
//        } else if (mode == ApplicationMode.LOCAL) {
//            System.out.println("Приложение запускается локально");
//        }

        switch (mode) {
            case DEV:
                System.out.println("Приложение запущено в dev среде");
                break;
            case LOCAL:
                System.out.println("Приложение запускается локально");
            // case LOCAL1:
            // case LOCAL2:
            // case LOCAL3:
                break;
            default:
                break;
        }

    }

}
