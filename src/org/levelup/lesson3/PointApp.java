package org.levelup.lesson3;

import java.util.Arrays;

public class PointApp {

    public static void main(String[] args) {
        for (int i = 0; i < args.length; i++) {
            System.out.print(args[i] + " ");
        }
        System.out.println();

        // System.out.println(Arrays.toString(args));

        // объявить переменную нашего типа
        // p - объект, ссылка, экземпляр, object, reference, instance
        // p и а - локальные переменные
        int a = 150;

        Point p = new Point(3, 5);
        // p.x = 3;
        // p.y = 5;

        System.out.println("Координаты точки (" + p.x + ", " + p.y + ")");

        Point leftPoint = new Point(10, 13);
        // leftPoint.x = 10;
        // leftPoint.y = 13;

        p.printPoint();
        leftPoint.printPoint(true);

        int leftPointQuadrant = leftPoint.quadrant();
        System.out.println("Квадрант точки leftPoint: " + leftPointQuadrant);

    }

}
