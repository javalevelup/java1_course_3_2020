package org.levelup.lesson9.exception;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParser {

    // String -> java.util.Date
    public Date fromString(String dateAsString) {
        // HH - 24 format 0 - 23
        // hh - 12 format (am/pm)
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

        try {
            System.out.println("Пытаюсь разобрать строку");
            Date date = sdf.parse(dateAsString);
            System.out.println("Строка успешно преобразована в дату");
            return date;
        } catch (ParseException exc) {
            System.out.println("Неправильная позиция: " + exc.getErrorOffset());
            return null;
        } finally {
            System.out.println("Завершаю работу метода fromString()");
        }
    }

}
