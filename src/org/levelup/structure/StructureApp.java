package org.levelup.structure;

public class StructureApp {

    public static void main(String[] args) {
        Structure s = new LinkList(); // new DynamicArray(3);
        // Structure s = new OneWayList();

        // DynamicArray arr = new DynamicArray(3);
        // Structure s = arr;

        s.add(15);
        s.add(16);
        s.add(785);
        s.add(543);
        s.add(945);
        s.add(623);
        s.add(88);
        s.add(45);

        System.out.println(s.getSize());
    }

}
