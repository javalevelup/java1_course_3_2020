package org.levelup.lesson2;

@SuppressWarnings("ALL")
public class DifferentTypes {

    public static void main(String[] args) {
        int intValue = 65;
        long longValue = 5490;

        // float floatValue = longValue / intValue; => 84,461538461538462
        // int result = longValue / intValue;

        System.out.println(longValue / intValue);

        double doubleValue = longValue;     // приведение типов
        long castedLongValue = intValue;    // расширяющее неявное преобразование

        int castedIntValue = (int) doubleValue; // сужающее явное преобразование

        double amount = 5945.99999;
        long longAmount = (long) amount;
        System.out.println(longAmount);

        // 127 -> 127
        // 128 -> -128
        // 129 -> -127
        // 130 -> -126

        // -129 -> 127
        // -130 -> 126
        // -131 -> 125

        // 384L (384l) - long
        // 384F (384f) - float
        // 384D (384d) - double
        byte byteValue = (byte) 384; // [-128, 127]
        System.out.println(byteValue);

        // byte a = 127;
        float floatValue = 454.443f;
    }

}
