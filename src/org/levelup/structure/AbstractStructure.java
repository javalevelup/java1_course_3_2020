package org.levelup.structure;

public abstract class AbstractStructure implements Structure {

    protected int size;

    @Override
    public int getSize() {
        return size;
    }

}
