package org.levelup.lesson10.lambda;

import org.levelup.lesson3.Point;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Lambda {

    public static void main(String[] args) {

        // Java 8
        //  FunctionalInterface
        //  StreamAPI
        //  Lambda functions

        int maxLength = 10;

        // Анонимный внутренний класс
        // Lambda$1
        Predicate<String> lengthPredicate = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.length() <= maxLength;
            }
        };

        // Lambda$2
//        new Predicate<String>() {
//            @Override
//            public boolean test(String s) {
//                return s.length() <= maxLength;
//            }
//        };
//
//        Collections.sort(new ArrayList<>(), new Comparator<Point>() {
//            @Override
//            public int compare(Point o1, Point o2) {
//                return 0;
//            }
//        });

        // Lambda функция - представляет переопределенный метод интерфейса

        // 1. Аргументы метода
        //  () - 0 аргументов
        //  (s), s - 1 аргумент, где s название аргумента
        //  (arg1, arg2) - 2 аргумента

        // 2. -> - переход от аргументов к реализации метода

        // 3. Тело метода
        //  { return ; },
        //  без фигурных скобок и не писать return

        Predicate<String> lambdaLengthPredicate = (string) -> string.length() <= maxLength;
        Predicate<String> lambda = someString -> {
            boolean notA = someString.charAt(0) != 'A';
            return someString.length() <= maxLength && notA;
        };

        // StreamAPI
        // Набор методов, которые облегчают работа с коллекциями

        Collection<MenuItem> menu = new ArrayList<>();
        menu.add(new MenuItem("Блюдо 1", 433.34));
        menu.add(new MenuItem("Блюдо 2", 127));
        menu.add(new MenuItem("Блюдо 3", 442.1));
        menu.add(new MenuItem("Блюдо 4", 785.1));
        menu.add(new MenuItem("Блюдо 5", 82.45));
        menu.add(new MenuItem("Блюдо 6", 1545.45));
        menu.add(new MenuItem("Блюдо 7", 852.5));
        menu.add(new MenuItem("Блюдо 8", 10.45));

        // Печать на экране
        menu.forEach(item -> System.out.println(item.name + " " + item.amount));

        // Стримы - потоки (последовательность) данных
        List<MenuItem> expensiveItems = menu.stream()
                .filter(item -> item.amount >= 1000) // в методе filter мы пишем условие, чтобы оставить в стриме
                .collect(Collectors.toList());

        System.out.println("Оригинальная коллекция");
        menu.forEach(item -> System.out.println(item.name + " " + item.amount));
        System.out.println("Список дорогих блюд");
        expensiveItems.forEach(item -> System.out.println(item.name + " " + item.amount));

        menu.stream()
                .map(item -> item.amount + " " + item.name) // Stream<String>
                .filter(s -> s.length() > 13)
                .forEach(s -> System.out.println(s));


        menu.stream()
                .sorted(Comparator.comparing(item -> item.amount, Comparator.reverseOrder()))
                .collect(Collectors.toList());
    }

    static class MenuItem {
        private String name;
        private double amount;

        public MenuItem(String name, double amount) {
            this.name = name;
            this.amount = amount;
        }
    }

}
