package org.levelup.lesson4;

import org.levelup.lesson3.Point;
import org.levelup.lesson3.TriplePoint;

@SuppressWarnings("ALL")
public class ObjectCast {

    public static void main(String[] args) {
        // Object <- Point <- TriplePoint
        Point p = new TriplePoint(); // TriplePoint tp = new TriplePoint(); Point p = tp;

        TriplePoint triplePoint = new TriplePoint();
        Point pointFromTP = triplePoint; // расширяющее

        Object objectFromPoint = pointFromTP;
        Object objectFromTP = triplePoint;

        TriplePoint tpFromObject = (TriplePoint) objectFromTP; // сужающее

        Point point = new Point();
        TriplePoint tpFromPoint = (TriplePoint) point; // ClassCastException

    }

}
