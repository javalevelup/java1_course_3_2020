package org.levelup.lesson10;

// Потоки ввода
// InputStream - считывает данные в виде массива byte
// Reader - считывает данные в виде массива char (String)

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

// Потоки вывода
// OutputStream - записывают данные в виде массива byte
// Writer - записывают данные в виде массива char (String)
public class FileService {

    // Два метода: read и write
    public void readAndDisplayFile(String filename) throws IOException {
        // FileInputStream - byte[]
        // FileReader - char[]
        // BufferedReader - String

        FileReader fr = new FileReader(filename);
        char[] buffer = new char[10];

        // int read(char[] buff) - возвращает количество символов, которые fr прочитал из файла
        // когда метод вернет -1, это означает, что мы дошли до конца файла
        while (fr.read(buffer) >= 0) {
            System.out.println(Arrays.toString(buffer));
        }
    }

    public void readAndDisplayFile(File file) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));

            String line;
            while ( (line = br.readLine()) != null ) {
                System.out.println(line);
            }

        } catch (IOException exc) {
            System.out.println("Ошибка чтения файла: " + exc.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException exc) {
                    System.out.println("Не удалось закрыть файл");
                }
            }
        }
    }

    public void writeToFile(String filename, List<String> text) {
        // FileService$1
//        Predicate<String> lengthPredicate = new Predicate<String>() {
//            @Override
//            public boolean test(String s) {
//                return s.length() < 100;
//            }
//        };
        Predicate<String> lengthPredicate = s -> s.length() < 100;

        // try-with-resources
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename, true))) {
            for (String line : text) {
                if (lengthPredicate.test(line)) {
                    // bw.write(line + "\n");
                    bw.write(line);
                    bw.newLine();
                }
            }
        } catch (IOException exc) {
            System.out.println("Ошибка записи в файл: " + exc.getMessage());
        }
    }

}
