package org.levelup.structure;

// Интерфейс/interface
// Нет конструкторов
// Все методы интерфейса - абстрактные и публичные (кроме методов с default)
// Вместо extends -> implements
// Все поля - public static final
// Один класс может реализовывать множество интерфейсов
public interface Structure {

    int getSize();

    void add(int value);

}
