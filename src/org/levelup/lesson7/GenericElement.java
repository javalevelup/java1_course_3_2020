package org.levelup.lesson7;

// Generics -> GenericClass/GenericMethod
// Параметризированный класс/метод

// <T> - alias (псевдоним) типа

// type erasure - стирание типов
public class GenericElement<TYPE> {

    private TYPE value;

    public GenericElement(TYPE value) {
        this.value = value;
    }

    public TYPE getValue() {
        return value;
    }

}
