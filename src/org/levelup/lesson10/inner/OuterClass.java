package org.levelup.lesson10.inner;

// Внешний класс
public class OuterClass {

    // Можно (и нужно) обращаться к полю без использования объекта класса
    // У статического поля всегда одно значение
    public static String objectName;

    private int privateInt;

    // Статические методы работают только со статическими переменными
    // Можно вызывать напрямую только другие статические методы
    public static void staticMethod() {
        System.out.println("Вызвали статический метод");
        OuterClass oClass = new OuterClass();
        oClass.displayUsingInnerClass();
    }

    private void privateMethod() {
        System.out.println("private method");
    }

    public void displayUsingInnerClass() {
        InnerClass innerClass = new InnerClass();
        innerClass.setPrivateInt(privateInt);
        innerClass.displayPrivateInt();
    }

    // Внутренние классы
    //  - Может иметь любой модификатор доступа
    //  - Может обращаться к любым методам и полям внешнего класса
    //  - Объект внутреннего класса не может существовать без объекта внешнего класса
    class InnerClass {

        public void setPrivateInt(int pInt) {
            privateInt = pInt;
        }

        public void displayPrivateInt() {
            privateMethod();
            System.out.println(privateInt);
        }

    }

    // Вложенный класс
    //  - Может иметь любой модификатор доступа
    //  - Может обращаться к любым статическим методам и статическим полям внешнего класса
    //  - Объект вложенного класса может существовать без объекта внешнего класса
    static class NestedClass /* extends OuterClass */ {

        public void methodInNestedClass() {
            staticMethod();
        }

    }

}
