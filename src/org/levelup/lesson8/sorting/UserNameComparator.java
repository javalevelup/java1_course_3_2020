package org.levelup.lesson8.sorting;

import java.util.Comparator;

public class UserNameComparator implements Comparator<User> {

    @Override
    public int compare(User o1, User o2) {
        if (o1.getName() == o2.getName()) {
            return 0;
        }
        if (o1.getName() == null) {
            return 1;
        }
        if (o2.getName() == null) {
            return -1;
        }

        return o1.getName().compareTo(o2.getName());
    }

}
