package org.levelup.lesson9.exception;

public class StackArray<T> {

    private Object[] stackArray;
    private int size;

    public StackArray(int stackSize) {
        // T[] stackArray = new T[stackSize];
        this.stackArray = new Object[stackSize];
    }

    // Положить объект в стэк
    public void push(T obj) {
        // что делать, если некуда добавлять?
        if (stackArray.length == size) {
            // Нам нужно выбросить исключение
            // Выброс исключения - генерация исключительной ситуации
            StackOverflowException exc = new StackOverflowException(obj);
            throw exc; // бросаем исключение
        }
        stackArray[size++] = obj;
        // stackArray[size] = obj;
        // size++; size = size + 1;
    }

    // Достать следующий элемент из стека (вершину стека, элемент на вершине стека)
    public T pop() throws EmptyStackException {
        // что делать, если ничего нет? если стэк пуст?
        if (size == 0) {
            throw new EmptyStackException();
        }

        // Object obj = stackArray[--size];
        // T t = (T) obj;
        // return t;
        //noinspection unchecked
        return (T) stackArray[--size];
    }

}
