package org.levelup.lesson4;

public class Person {

    private int age; // Теперь нельзя так сделать p.age = 43; p.age = -3;

    public void changeAge(int age) {
        if (age > 0) {
            this.age = age;
        } else {
            System.out.println("Нельзя использовать отрицательный возраст");
        }
    }

    // getter
    // public <field type> get<FieldName>() { return <field>; }
    public int getAge() {
        return age;
    }

}
