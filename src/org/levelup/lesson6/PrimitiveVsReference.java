package org.levelup.lesson6;

import org.levelup.lesson3.Point;

@SuppressWarnings("ALL")
public class PrimitiveVsReference {

    public static void main(String[] args) {
        int variable = 10; // 65474
        Point point = new Point(10, 10); // 86945

        variable = changeInt(variable);
        // variable -> 65474 -> 20

        int previousX = point.x; // previous - 10
        changeXPoint(point);

        System.out.println(variable);            // 10
        System.out.println(point.x);             // 20: point - 86945, point.x -> 86945.1
    }

    // pass by value

    static int changeInt(int var) { // var = 12345
        var = 20; // 12345 -> 20
        return var; // 12345
    }

    static void changeXPoint(Point point) { // 86945
        // point = new Point();
        point.x = 20; // x -> 86945.1 -> 20
    }

}
