package org.levelup.lesson5;

import org.levelup.lesson3.Point;

import java.nio.channels.Pipe;

public class FigureApp {

    public static void main(String[] args) {
        Shape shape = new Shape(new Point[0]);
        System.out.println("Площадь фигуры " + shape.calculateSquare());

        Rectangle rectangle = new Rectangle(
            new Point(3, 5), // left top
            new Point(5, 1)  // right bottom
        );
        // width - 2
        // height - 4

        System.out.println("Площадь прямоугольника " + rectangle.calculateSquare());

        // Приведение ссылочных типов
        // Rectangle r = new Rectangle(...);
        // Shape s = r;

        Shape rectangleShape = new Rectangle(
                new Point(4, 6),
                new Point(7, 1)
        ); // расширяющее преобразование - внутри находится объект класса Rectangle
        System.out.println("Площадь фигуры (была прямоугольником): " + rectangleShape.calculateSquare());
        // Методы getWight() и getHeight() у rectangleShape недоступны
        Rectangle fromRectangleShape = (Rectangle) rectangleShape; // сужающее преобразование
        // Методы getWight() и getHeight() у fromRectangleShape снова доступны

        // Rectangle fromShape = (Rectangle) shape; -> ClassCastException

        Shape[] shapes = new Shape[] {
            shape,
            rectangle, // (Shape) shape
            rectangleShape,
            new Triangle(new Point(), new Point(), new Point()),
            new Rectangle(new Point(1, 10), new Point(10, 1))
        };
        // shapes = [s, r, r, t, r]

        System.out.println();
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].calculateSquare());
        }

        Shape ts = new Triangle(null, null, null);
        // ts - является объектом класса Triangle
        // ts - имеет тип Shape
        ts.calculateSquare();

    }

    static void printSquares(Shape[] shapes) {
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].calculateSquare());
        }
    }

    static void printSquares(Rectangle[] rectangles) {
        for (int i = 0; i < rectangles.length; i++) {
            System.out.println(rectangles[i].calculateSquare());
        }
    }

}
