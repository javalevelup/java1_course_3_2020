package org.levelup.lesson8.sorting;

import java.util.Objects;

// Интерфейсы для сортировки
//  1. Comparable - используется для того, чтобы сделать класс сортируемым
//      Этот интерфейс должен реализовываться самим классом, объекты которого будут сортироваться
//  2. Comparator - используется для задания сортировки объектов
public class User implements Comparable<User> {

    private String login;
    private String name;
    private int age;

    public User(String login, String name, int age) {
        this.login = login;
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return age == user.age &&
                Objects.equals(login, user.login) &&
                Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, name, age);
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    // Аналог equals
    // user1.compareTo(user2)
    //  < 0 -> user1 < user2
    //  = 0 -> user1 = user2
    //  > 0 -> user1 > user2
    @Override
    public int compareTo(User o) {
        // сортировка по логину
        if (login == o.login) {
            return 0;
        }
        if (login == null) {
            return 1; // все null - будут в конце коллекции (к примеру)
        }
        if (o.login == null) {
            return -1;
        }

        // return Integer.compare(age, o.age);
        return login.compareTo(o.login); // сортировка по алфавиту
        // return -login.compareTo(o.login); - сортировка по алфавиту по убыванию (в обратном порядке)
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

}

