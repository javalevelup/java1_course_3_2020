package org.levelup.lesson8;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

// HashMap <- LinkedHashMap
// interface NavigableMap <- interface SortedMap <- TreeMap
public class HashMapApp {

    public static void main(String[] args) {

        // набор пар ключ-значение
        // ассоциативный массив
        Map<String, String> shortUrls = new HashMap<>(); // get("") -> O(1)

        shortUrls.put("products", "https://market.yandex.ru");
        shortUrls.put("books", "https://labirint.ru");
        shortUrls.put("cars", "https://autospot.ru");
        shortUrls.put("buycar", "https://auto.ru");

        String url = shortUrls.get("products");
        System.out.println("products:" + url);

        String obj = shortUrls.get("book"); // null
        System.out.println(obj);

        shortUrls.put("buycar", "https://avito.ru"); //3случай [] : ("buycar", "auto.ru") -> ("buycar2", "avito.ru")

        // 1var
        Set<String> keys = shortUrls.keySet(); // набор ключей (множество ключей), которые есть в Map
        for (String key : keys) {
            System.out.println("Key: " + key + ", value: " + shortUrls.get(key));
        }

        System.out.println();

        // 2var
        Set<Map.Entry<String, String>> entries = shortUrls.entrySet(); // entry - пара ключ-значения (объект у которого есть методы для обращения к ключу или значению)
        for (Map.Entry<String, String> entry : entries) { // ("buycar", "auto.ru"), ("buycar2", "avito.ru")
            System.out.println("Key: " + entry.getKey() + ", value: " + entry.getValue());
        }

        System.out.println();

        Collection<String> values = shortUrls.values(); // набор значений
        for (String value : values) {
            System.out.println("Value: " + value);
        }

        System.out.println();

        shortUrls.forEach((key, value) -> System.out.println("Key: " + key + ", value: " + value));

//        int[] arr = new int[4];
//        arr[3] = 40;
//        // ...
//        arr[3] = 50;


    }

}
