package org.levelup.lesson3;

public class OopExamples {

    public static void main(String[] args) {
        // Object <- Point
        Point point = new Point();
        //  Object <- Point <- TriplePoint
        TriplePoint triplePoint = new TriplePoint();

        point.x = 1;
        point.y = 40;
        point.printPoint();

        triplePoint.x = 10;

        triplePoint.y = 43;
        triplePoint.setZ(23);

        triplePoint.printPoint();

        triplePoint.changeCoordinates(4, 32, 42);
        triplePoint.printPoint();

        // System.out.println(triplePoint.y + " " + triplePoint.getZ());
    }

}
