package org.levelup.lesson4;

public class Calculator {

    public static void main(String[] args) {
        double result = add(5, 5);
        System.out.println(result);
    }

    public static double add(double a, double b) {
        return a + b;
    }

}
