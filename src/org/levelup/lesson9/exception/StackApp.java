package org.levelup.lesson9.exception;

public class StackApp {

    public static void main(String[] args) {
        StackArray<Integer> stack = new StackArray<>(3);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        // stack.push(6);
        // stack.push(7);

        try {
            stack.pop();
            stack.pop();
            stack.pop();
            stack.pop();
        } catch (EmptyStackException exc) {
            exc.printStackTrace();
        }
    }

}
