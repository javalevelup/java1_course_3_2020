package org.levelup.lesson8;

// mutable/immutable - изменяемый/неизменяемый (объект)
// 1. Значения полей задаются в конструкторе
// 2. Нет сеттеров
// 3. Поля объявлены как final
// 4. Класс объявлен как final
// 5. Методы, которые как-то изменяют внутреннее состояние объекта
//          (хотят изменить значения полей), возвращают новый объект
public final class ImmutableUser { // final class - Нельзя наследоваться от этого класса

    private final String username;
    private final String email;

    // ImmutableUser user = new ImmutableUser("us", "email@email.com");
    // user.setUsername("username"); -> неверно!
    public ImmutableUser(String username, String email) {
        this.username = username;
        this.email = email;
    }

    public final String getEmail() { // final method - метод, который нельзя переопределить
        return email;
    }

    public String getUsername() {
        return username;
    }

    public ImmutableUser changeEmail(String email) {
        return new ImmutableUser(username, email);
    }

}
