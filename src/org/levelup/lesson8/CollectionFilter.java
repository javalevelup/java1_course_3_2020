package org.levelup.lesson8;

import java.util.LinkedList;
import java.util.function.Predicate;

public class CollectionFilter<E> {

    public static <E> LinkedList<E> filter(LinkedList<E> coll, Predicate<E> filter) {
        return null;
    }

    public <T> void method(T obj) {

    }

    public <T> T[] array(T[] array) {
        Object[] arr = new Object[4]; // массив когерентен
        arr[0] = array[0];
        // T[] ts = new T[4];
        // T o = new T(3, 4); // Object o = new Object();
        return (T[]) arr;
    }

    public static void main(String[] args) {
        CollectionFilter<String> filter = new CollectionFilter<>();
        filter.method(50);
        filter.method(new Object());
        filter.method("");

        filter.array(new Integer[] {3, 4, 5 });
        filter.array(new String[] {"3", "4", "5" });

        LinkedList<Integer> integers = filter.filter(new LinkedList<Integer>(), new Predicate<Integer>() {
            @Override
            public boolean test(Integer integer) {
                return false;
            }
        });
    }

}
