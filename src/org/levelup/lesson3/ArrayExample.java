package org.levelup.lesson3;

public class ArrayExample {

    public static void main(String[] args) {
        // объявление массива
        // int[] arr;
        int var = 5;

        int[] arr = new int[5]; // инициализация массива
        arr[0] = 15;
        arr[2] = 46;
        // arr[5] = 45; ArrayIndexOutOfBoundsException: index >= arr.length or index < 0
        System.out.println("Первый элемент: " + arr[0]);
        System.out.println("Третий элемент: " + arr[2]);

        // Вывести массив на эран
        for (int index = 0; index < arr.length; index++) {
            // 0 -> arr[0]
            // 1 -> arr[1]
            // 4 -> arr[4]
            // sout + Tab
            // psvm + Tab
            System.out.println("Индекс: " + index + ", значение: " + arr[index]);
        }

        // Сокращенное форма инициализации массива
//        int[] v = new int[6];
//        v[0] = 3;
//        v[1] = 4;
//        v[2] = 5;
//        v[3] = 7;
//        v[4] = 8;
//        v[5] = 9;
        int[] vars = { 3, 4, 5, 7, 8, 9 }; // [3, 4, 5, 7, 8, 9]

    }

}
