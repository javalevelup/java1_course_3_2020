package org.levelup.lesson5;

import org.levelup.lesson3.Point;

// Это базовый класс для всех фигур
public class Shape {

    // Каждая фигура представлена точками
    protected Point[] points;

    public Shape(Point[] points) {
        this.points = points;
    }

    public double calculateSquare() {
        return .0d; // 0.0d, 0.d, .0d
    }

}
