package org.levelup.lesson7;

import org.levelup.lesson3.Point;

public class GenericExample {

    public static void main(String[] args) {
//        GenericElement element = new GenericElement("some string");
//        Integer i = (Integer) element.getValue();

        GenericElement<Integer> intGenericValue =
                new GenericElement<>(4354);
        Integer i = intGenericValue.getValue();

        GenericElement<String> stringGenericValue =
                new GenericElement<>("some string");
        String s = stringGenericValue.getValue();

        GenericElement<Point> pointGenericValue =
                new GenericElement<>(new Point(4 ,2));
        Point p = pointGenericValue.getValue();

    }

}
