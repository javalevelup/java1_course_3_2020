package org.levelup.lesson2;

@SuppressWarnings("ALL")
public class IncrementDecrement {

    public static void main(String[] args) {
        int value = 10;

//        value++; // value = value + 1
//        value--; // value = value - 1

        // value++; // постфиксный инкремент - сначала выполнить другие операции, потом сделать инкремент
        // ++value; // префиксный инкремент - сначала сделать инкремент, потом выполнить другие операции

        System.out.println(value++); // sout(value); value = value + 1
        System.out.println(++value); // value = value + 1; sout(value)

        value = 10;
        int v = 10;
        int c = 10;

        int r1 = v + c + value++; // 30
        int r2 = v + c + ++value; // 32

        System.out.println("Сумма v и c: " + (v + c));
        System.out.println(v + c + " : сумма v и c");
    }

}
