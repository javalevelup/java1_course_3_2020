package org.levelup.structure;

import java.util.Iterator;

public class LinkListIterator implements Iterator<Integer> {

    private Element current;

    public LinkListIterator(Element start) {
        current = start;
    }

    @Override
    public boolean hasNext() {
        return current != null;
    }

    @Override
    public Integer next() {
        int value = current.getIntValue();
        current = current.getNext();
        return value; // return Integer.valueOf(value);
    }

}
