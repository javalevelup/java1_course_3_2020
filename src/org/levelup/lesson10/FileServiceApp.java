package org.levelup.lesson10;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileServiceApp {

    public static void main(String[] args) throws IOException {
        FileService fs = new FileService();
        // fs.readAndDisplayFile("test-file.txt");
        fs.readAndDisplayFile(new File("test-file.txt"));

        List<String> lines = new ArrayList<>();
        lines.add("text1");
        lines.add("text2");
        lines.add("text3");
        lines.add("text4");

        fs.writeToFile("test-file.txt",  lines);

    }

}
