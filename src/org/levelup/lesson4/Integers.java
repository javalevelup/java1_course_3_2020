package org.levelup.lesson4;

@SuppressWarnings("ALL")
public class Integers {

    public static void main(String[] args) {
        Integer i1 = 126; // Integer.valueOf(126);
        Integer i2 = Integer.valueOf(126);
        Integer i3 = 128;
        Integer i4 = 128;

        // true/false

        // false/false
        // true/true
        System.out.println(i1 == i2);
        System.out.println(i3 == i4);
    }

}
